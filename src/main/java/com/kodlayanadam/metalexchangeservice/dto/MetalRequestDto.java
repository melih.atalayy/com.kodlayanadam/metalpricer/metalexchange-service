package com.kodlayanadam.metalexchangeservice.dto;

import com.kodlayanadam.common.dto.BaseDto;
import com.kodlayanadam.common.enums.CurrencyCode;
import com.kodlayanadam.common.enums.MetalSymbol;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MetalRequestDto extends BaseDto {

    @NotNull
    private MetalSymbol metalSymbol;

    @NotNull
    private CurrencyCode currencyCode;
}
