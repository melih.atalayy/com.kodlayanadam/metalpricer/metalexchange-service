package com.kodlayanadam.metalexchangeservice.dto;

import com.kodlayanadam.common.dto.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MetalResponseDto extends BaseDto {

    private String metal;
    private String currency;
    private Double price;
    private Double priceGram24K;
    private Double priceGram22K;
    private Double priceGram21K;
    private Double priceGram20K;
    private Double priceGram18K;
    private Double priceGram16K;
    private Double priceGram14K;
    private Double priceGram10K;
}
