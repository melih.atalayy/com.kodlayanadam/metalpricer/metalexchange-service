package com.kodlayanadam.metalexchangeservice.service;

import com.kodlayanadam.common.dto.ServiceRequest;
import com.kodlayanadam.common.dto.ServiceResponse;
import com.kodlayanadam.common.enums.ServiceEnum;
import com.kodlayanadam.exception.ServiceException;
import com.kodlayanadam.metalexchangeservice.client.MetalCurrentClient;
import com.kodlayanadam.metalexchangeservice.dto.MetalRequestDto;
import com.kodlayanadam.metalexchangeservice.dto.MetalResponseDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MetalExchangeServiceImpl implements MetalExchangeService{

    private final MetalCurrentClient client;

    public MetalExchangeServiceImpl(MetalCurrentClient client) {
        this.client = client;
    }

    @Transactional
    @Override
    public ServiceResponse<MetalResponseDto> getMetalPrice(ServiceRequest<MetalRequestDto> request) {
        try {
            return client.retrieveMetalPriceFromCurrentService(request);
        }catch (ServiceException e){
            throw new ServiceException("Client call problem.!", ServiceEnum.NOT_OK);
        }
    }
}
