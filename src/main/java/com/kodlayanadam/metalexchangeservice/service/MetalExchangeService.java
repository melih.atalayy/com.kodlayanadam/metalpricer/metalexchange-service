package com.kodlayanadam.metalexchangeservice.service;

import com.kodlayanadam.common.dto.ServiceRequest;
import com.kodlayanadam.common.dto.ServiceResponse;
import com.kodlayanadam.metalexchangeservice.dto.MetalRequestDto;
import com.kodlayanadam.metalexchangeservice.dto.MetalResponseDto;

public interface MetalExchangeService {

    ServiceResponse<MetalResponseDto> getMetalPrice(ServiceRequest<MetalRequestDto> request);
}
