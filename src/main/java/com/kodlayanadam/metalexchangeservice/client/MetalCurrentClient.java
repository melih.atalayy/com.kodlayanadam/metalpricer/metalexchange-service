package com.kodlayanadam.metalexchangeservice.client;


import com.kodlayanadam.common.dto.ServiceRequest;
import com.kodlayanadam.common.dto.ServiceResponse;
import com.kodlayanadam.metalexchangeservice.dto.MetalRequestDto;
import com.kodlayanadam.metalexchangeservice.dto.MetalResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "metalcurrent-service",url = "http://localhost:7001")
public interface MetalCurrentClient {

    @PostMapping("/metal/current/metal-price")
    ServiceResponse<MetalResponseDto> retrieveMetalPriceFromCurrentService(
            @RequestBody ServiceRequest<MetalRequestDto> request
    );
}