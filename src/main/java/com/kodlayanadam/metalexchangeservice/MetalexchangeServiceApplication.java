package com.kodlayanadam.metalexchangeservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = "com.kodlayanadam.**")
@EnableFeignClients
public class MetalexchangeServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MetalexchangeServiceApplication.class, args);
    }

}
