package com.kodlayanadam.metalexchangeservice.controller;

import com.kodlayanadam.common.dto.ServiceRequest;
import com.kodlayanadam.common.dto.ServiceResponse;
import com.kodlayanadam.metalexchangeservice.dto.MetalRequestDto;
import com.kodlayanadam.metalexchangeservice.dto.MetalResponseDto;
import com.kodlayanadam.metalexchangeservice.service.MetalExchangeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/metal/exchange")
public class MetalExchangeController {

    private final MetalExchangeService service;

    public MetalExchangeController(MetalExchangeService service) {
        this.service = service;
    }

    @PostMapping("/metal-price")
    public ResponseEntity<ServiceResponse<MetalResponseDto>> getExchangeMetalPrice(@RequestBody ServiceRequest<MetalRequestDto> request){
        return ResponseEntity.ok(service.getMetalPrice(request));
    }

}
