package com.kodlayanadam.metalexchangeservice.repository;

import com.kodlayanadam.metalexchangeservice.entity.MetalPrice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface MetalPriceRepository extends JpaRepository<MetalPrice,Long> {

    Collection<MetalPrice> findTop10ByOrderByFetchTimeDesc();
}
